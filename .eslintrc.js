module.exports = {
  env: {
    node: true,
    es6: true
  },
  extends: [
    'plugin:vue/recommended',
    'plugin:prettier/recommended',
    'eslint:recommended'
  ],
  parser: 'vue-eslint-parser',
  plugins: ['gridsome'],
  rules: {
    'prettier/prettier': ['error', { singleQuote: true }]
  }
};
